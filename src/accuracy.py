__author__ = 'vinaykumar'

import sys

fin = open(sys.argv[1], 'r')
fout = open(sys.argv[2], 'r')

correct = fin.read().split()
wrong = fout.read().split()

i = 0
errors = 0
counter = 0

label = ["it's", "its", "you're", "your", "they're", "their", "loose", "lose", "too", "to"]

while i < len(correct):
    counter += 1
    if correct[i] != wrong[i] and correct[i] in label:
        errors += 1
        print(correct[i], " ", wrong[i])
    i += 1

print(counter, " ", errors)
fin.close()
fout.close()