__author__ = 'vinaykumar'

import sys
import pickle
import nltk
from nltk.corpus import brown

fin = open(sys.argv[1], 'r')
fout = open(sys.argv[2], 'wb')

training = fin.read().split()

training_tagged = nltk.pos_tag(training)

pickle.dump((training_tagged), fout)

fout.close()