__author__ = 'vinaykumar'

import sys
import pickle

fin = open(sys.argv[1], 'rb')
fout = open(sys.argv[2], 'w')

training_tagged = pickle.load(fin)

label = ["it's", "its"]

length = len(training_tagged)

for i in range(0, length):
    if training_tagged[i][0] in label:
        word = training_tagged[i][0]
        tag = training_tagged[i][1]

        if i >=1:
            pword1 = training_tagged[i-1][0]
            ptag1 = training_tagged[i-1][1]

        else:
            pword1 = "L"
            ptag1 = "L"

        if i >=2:
            pword2 = training_tagged[i-2][0]
            ptag2 = training_tagged[i-2][1]

        else:
            pword2 = "L"
            ptag2 = "L"

        if i >=3:
            pword3 = training_tagged[i-3][0]
            ptag3 = training_tagged[i-3][1]

        else:
            pword3 = "L"
            ptag3 = "L"

        if i <= (length-2):
            nword1 = training_tagged[i+1][0]
            ntag1 = training_tagged[i+1][1]

        else:
            nword1 = "R"
            ntag1 = "R"

        if i <= (length-3):
            nword2 = training_tagged[i+2][0]
            ntag2 = training_tagged[i+2][1]

        else:
            nword2 = "R"
            ntag2 = "R"

        if i <= (length-4):
            nword3 = training_tagged[i+3][0]
            ntag3 = training_tagged[i+3][1]

        else:
            nword3 = "R"
            ntag3 = "R"

        fout.write(word+" pword1:"+pword1+" ptag1:"+ptag1+" pword2:"+pword2+" ptag2:"+ptag2+" nword1:"+nword1+" ntag1:"+ntag1+" nword2:"+nword2+" ntag2:"+ntag2+"\n")

fout .close()
