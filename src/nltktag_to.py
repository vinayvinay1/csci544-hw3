__author__ = 'vinaykumar'

import sys
import pickle
import nltk


fmodel = open(sys.argv[1], 'rb')
fin = open(sys.argv[2], 'r')
fintagged = open(sys.argv[3], 'rb')
fout = open(sys.argv[4], 'w')

wavg, headers = pickle.load(fmodel)
fmodel.close()

test = fin.read()
test_tagged = pickle.load(fintagged)

lines = test.splitlines()

classes = {}
output = []

for h in headers:
    classes.update({h: 0})

label = ["too", "to"]

length = len(test_tagged)

for i in range(0, length):
    if test_tagged[i][0] in label:
        word = test_tagged[i][0]
        tag = test_tagged[i][1]

        if i >=1:
            pword1 = test_tagged[i-1][0]
            ptag1 = test_tagged[i-1][1]

        else:
            pword1 = "L"
            ptag1 = "L"

        if i >=2:
            pword2 = test_tagged[i-2][0]
            ptag2 = test_tagged[i-2][1]

        else:
            pword2 = "L"
            ptag2 = "L"

        if i >=3:
            pword3 = test_tagged[i-3][0]
            ptag3 = test_tagged[i-3][1]

        else:
            pword3 = "L"
            ptag3 = "L"

        if i <= (length-2):
            nword1 = test_tagged[i+1][0]
            ntag1 = test_tagged[i+1][1]

        else:
            nword1 = "R"
            ntag1 = "R"

        if i <= (length-3):
            nword2 = test_tagged[i+2][0]
            ntag2 = test_tagged[i+2][1]

        else:
            nword2 = "R"
            ntag2 = "R"

        if i <= (length-4):
            nword3 = test_tagged[i+3][0]
            ntag3 = test_tagged[i+3][1]

        else:
            nword3 = "R"
            ntag3 = "R"

        formatted_word = "pword1:"+pword1+" ptag1:"+ptag1+" pword2:"+pword2+" ptag2:"+ptag2+" nword1:"+nword1+" ntag1:"+ntag1+" nword2:"+nword2+" ntag2:"+ntag2+"\n"

        op_classes = dict(classes)
        for h in headers:
            for word in formatted_word.split():
                if word in wavg[h]:
                    op_classes[h] += wavg[h][word]

        cword = max(op_classes, key=op_classes.get)
        output.append(cword)

counter = 0
for line in lines:
    words = line.split()
    for word in words:
        if word in label:
            fout.write(output[counter])
            fout.write(" ")
            counter += 1

        else:
            fout.write(word)
            fout.write(" ")

    fout.write("\n")

fmodel.close()
fin.close()
fout.close()
fintagged.close()