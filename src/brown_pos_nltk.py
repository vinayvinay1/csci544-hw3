__author__ = 'vinaykumar'

import sys
import pickle
import nltk
from nltk.corpus import brown

fout = open(sys.argv[1], 'wb')

training = brown.words()

training_tagged = nltk.pos_tag(training)

pickle.dump((training_tagged), fout)

fout.close()