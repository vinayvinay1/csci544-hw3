Approach:

I have used POS tagging to tag Training corpus and usedmy Averaged Perceptron to predict and make the corrections for homophones.

Data used : NLTK Brown corpus + HW3 dev data

Third-party software used : NLTK library

I have used 5 different NLTK Train and Tag scripts for each pair of homophones.


Dev correction statistics :
						Words found Wrong		Words Corrected			Accuracy
TO and TOO 				6127					5778					94.3
YOUR and YOU'RE			255						230						90.2
THEIR and THEY'RE		590						576						97.6
LOSE and LOOSE			18						16						88.8
ITS and IT'S 			408						377						92.4

TOTAL					7398					6977					94.3		
